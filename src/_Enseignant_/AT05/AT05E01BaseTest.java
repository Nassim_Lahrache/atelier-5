package _Enseignant_.AT05;

import static org.junit.Assert.*;

import org.junit.Test;

public class AT05E01BaseTest {

	@Test
	public void testFusionVec() {
		
		long[] tab3 = {2, 4, 3, 5, 1, 7, 6};
		long[] tab4 = {3, 7, 2, 8, 9, 5};
		long[] tabF2 = new long[13];
		long[] tabFRep2 = {2, 4, 3, 5, 1, 7, 6, 3, 7, 2, 8, 9, 5};
		
		assertEquals(13, AT05E01Base.fusionVec(tab3, 7, tab4, 6, tabF2, false));
		assertArrayEquals(tabF2, tabFRep2);
	}

	@Test
	public void testTri() {

		long[] tab1 = { 5, 2, 3, 7, 1, 4, 6, 8 };
		long[] tabRep = { 1, 2, 3, 4, 5, 6, 7, 8 };

		AT05E01Base.triSel(tab1, 8);
		assertArrayEquals(tabRep, tab1);

	}

	@Test
	public void testAjout() {

		long[] tab1 = {1, 2, 4, 5, 6, 7, -1, -1};
		long elem = 3;
		long[] tabRep = {1, 2, 3, 4, 5, 6, 7, -1};
		
		assertEquals(7, AT05E01Base.ajout(tab1,  6, elem, true));
		assertArrayEquals(tabRep, tab1);
		
		
		long[] tab2 = {7, 6, 4, 3, 2, 1, -1, -1};
		long elem2 = 5;
		long[] tabRep2 = {7, 6, 4, 3, 2, 1, 5, -1};
		
		assertEquals(7, AT05E01Base.ajout(tab2, 6, elem2, true));
		assertArrayEquals(tabRep2, tab2);
		
	}

	@Test
	public void testRechercheBin() {

		long[] tab1 = {1, 4, 7, 3, 6, 9};

		assertEquals(2, AT05E01Base.rechercheBin(tab1, 6, 7));

	}

	@Test
	public void testRetrait() {
		
		long[] tab1 = {2, 3, 1, 6, 4, 7, 5};
		long[] tabRep = {2, 3, 1, 5, 4, 7, -1};
		
		assertEquals(6, AT05E01Base.retrait(tab1, 7, 6, 'D'));
		assertArrayEquals(tabRep, tab1);

	}

}